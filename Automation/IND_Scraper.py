from cStringIO import StringIO
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
import re
import openpyxl
import os
import datetime
import PyPDF2

def pdf_to_text(fname, pages=(0)):
    if not pages:
        pagenums = set()
    else:
        pagenums = set(pages)

    output = StringIO()
    manager = PDFResourceManager()
    converter = TextConverter(manager, output, laparams=LAParams())
    interpreter = PDFPageInterpreter(manager, converter)

    infile = file(fname, 'rb')
    for page in PDFPage.get_pages(infile, pagenums):
        interpreter.process_page(page)
    infile.close()
    converter.close()
    text = output.getvalue()
    output.close()
    return text[:text.find('Dear')]


def spider(path, fname):
    page = pdf_to_text(path + '\\' + fname)
    data = [re.compile(r'MCN No[\s:#]+([\w-]+)').search(page).group(1),  #0: MCN
            re.compile(r'[\[\(](?!Ibrutinib|ibrutinib|s|IRB|UPIRSOs|IEC|ISF)([\w\s\,\-]+)[\]\)]', re.DOTALL).findall(page),  #1: Event terms
            re.compile(r'\d{2}[\s-]?\D{3,9}[\s-]?\d{4}').findall(page)[0],  #2: Date
            re.compile(r'Status\:? (.*)\s?\(Day 0:\s?(.*)\)').search(page).group(2),  #3: Version date
            fname.split()[1]]   #4: Status

    events = ', '.join(data[1]).replace('\n', '').split(', ')
    data[1] = ', '.join(set(events))
    
    DATE_FORMATS = ['%d %B %Y', '%d %b %Y', '%d-%B-%Y', '%d-%b-%Y']
    for date_format in DATE_FORMATS:
        try:
            data[2] = datetime.datetime.strptime(data[2], date_format).strftime('%m/%d/%Y')
        except ValueError:
            pass
        else:
            break
    else:
        data[2] = None
    
    try:
        data[3] = datetime.datetime.strptime(data[3], '%d-%b-%Y').strftime('%m/%d/%Y')
    except ValueError:
        data[3] = data[2]

    return data

'''
def write_excel(data, fname="IND Tracker.xlsx"):
    try:
        wb = openpyxl.load_workbook(fname)
    except IOError:
        wb = openpyxl.Workbook()
    ws = wb.active
    ws.append(data)
    wb.save(filename=fname)

    # TODO: select correct month sheet
    month = datetime.datetime.strptime(data[2], '%m/%d/%Y').month
'''

def iterDir(path):
    for i in os.listdir(path):
        if i.endswith('.pdf'):
            yield (path + '\\', i)


def rename(data, path, fname):
    # Input is data returned from spider()
    data[2] = datetime.datetime.strptime(data[2], '%m/%d/%Y').strftime('%Y%b%d')
    new_name = "{0} {1} - {2}.pdf".format(data[2], data[4], data[1])
    os.rename(path + '\\' + fname, path + '\\' + new_name)


def overlay():
    with open("IND2.pdf", "rb") as inFile, open("Label.pdf", "rb") as overlay:
        original = PyPDF2.PdfFileReader(inFile)
        background = original.getPage(0)
        foreground = PyPDF2.PdfFileReader(overlay).getPage(0)

    # merge the first two pages
    background.mergePage(foreground)

    # add all pages to a writer
    writer = PyPDF2.PdfFileWriter()
    for i in range(original.getNumPages()):
        page = original.getPage(i)
        writer.addPage(page)

    # write everything in the writer to a file
    with open("modified.pdf", "wb") as outFile:
        writer.write(outFile)


def main():
    ind_path = 'C:\\Users\\jchung2\\Desktop\\IND\\PCYC-1121-CA\\Safety Reports'
    wbname = 'IND Tracker.xlsx'

    try:    # open excel file
        wb = openpyxl.load_workbook(ind_path + "\\" + wbname)
    except IOError:
        wb = openpyxl.Workbook()
    ws = wb.active

    for path, fname in iterDir(ind_path):   # Iterate over everything
        data = spider(path, fname)
        ws.append(data)
        rename(data, path, fname)

    wb.save(filename = ind_path + "\\" + wbname)


#if __name__ == "__main__":
#    main()


# TODO: function for writing into word document (pdf forms) for stickers



