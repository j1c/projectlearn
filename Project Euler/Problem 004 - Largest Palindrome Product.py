from timeit import default_timer as timer

def original():
    start = timer()

    l = []
    for i in range(100,1000):
        for j in range(100,1000):
            s = str(i*j)
            if s == s[::-1]:
                l.append(i*j)
    print max(l)
    elapsed_time = timer() - start
    print elapsed_time


def optimize1():    # Start top down to find the largest first.
    start = timer()
    for i in range(999,99,-1):
        for j in range(999,99,-1):
            s = str(i*j)
            if s == s[::-1]:
                print i*j
                break

    elapsed_time = timer() - start
    print elapsed_time

original()
optimize1()
