__author__ = 'Jaewon'


def is_prime(x):
    if x == 1 or x == 2 or x == 3:
        return True
    else:
        l = range(3, x, 2)
        for i in l:
            if x % i == 0:
                return False
                break
        return True


def generate_primes():
    primes = [2]
    number = 3
    while len(primes) < 1000:
        if is_prime(number) == True:
            primes.append(number)
        number += 2
    return primes

print generate_primes()